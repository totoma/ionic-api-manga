export class Manga {
    name: String;
    author : String;
    url: String;

    constructor(name: String, author: String, url: String){
        this.name = name;
        this.author = author;
        this.url = url;
    }
}