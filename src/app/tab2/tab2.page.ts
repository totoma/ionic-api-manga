import { Component, OnInit } from '@angular/core';
import { MangaService } from '../services/manga.service';
import { typeMangaService } from '../services/type-manga.service';


@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {
  name: any;
  author: any;
  url: any;
  selectTypeManga: any;
  mangas: any;
  TypeManga: any;

  constructor(private MangaService: MangaService, private typeMangaService: typeMangaService) {}

  ngOnInit(){
    this.MangaService.getAllManga().subscribe((result:any) => {
      console.log(result);
      this.mangas = result.mangas;

    this.typeMangaService.getAllTypeManga().subscribe(selectTypeManga => {
      console.log(selectTypeManga);
      this.selectTypeManga = selectTypeManga;
    })
  })
}

  createManga(){
    console.log(this.TypeManga);
    
    const body = {
      name: this.name,
      author: this.author,
      id_type_manga: this.TypeManga,
      url: this.url
    }
    this.MangaService.postData(body)
    .subscribe(response => {
      console.log(response);
    })
  }

  recupIdTypeManga(id: any){
    this.TypeManga = id
  }
}