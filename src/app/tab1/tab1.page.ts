import { Component, OnInit, ViewChild } from '@angular/core';
import { MangaService } from '../services/manga.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  result: any;
  resultGet: any;
  resultDelete: any;
  name: any;
  id: any;

  constructor(public MangaService : MangaService) {}

  ngOnInit(){
    this.getAllManga();
  }

  getAllManga(){
    this.MangaService.getAllManga().subscribe(res=>{
      this.result = res;
    })
  }

  getManga(test: any){
    this.MangaService.idTaken = test
    this.MangaService.getManga(test).subscribe(res=>{
      this.resultGet = res;
    })
  }

  deleteManga(id: any){
    this.MangaService.deleteManga(id).subscribe(res=>{
      this.resultDelete = res;
      this.ngOnInit();
    })
  }
}
