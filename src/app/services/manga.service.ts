import { Injectable } from "@angular/core";
import {HttpClient} from "@angular/common/http";
import { Manga } from "../models/Manga";

@Injectable({
    providedIn: 'root'
})
    export class MangaService {

        idTaken: any;

        url = "http://localhost:8000"

        constructor(private http: HttpClient){
            
        }

        getAllManga(){
            return this.http.get(this.url +'/manga/getAllMangas')
        }

        postData(data: any){
            return this.http.post(this.url +'/manga/createManga', data)
        }

        updateManga(data: any){
            return this.http.put(this.url +'/manga/updateManga',data)
        }

        getManga(id: any){
            return this.http.get(this.url +'/manga/getManga/'+ id)
        }

        deleteManga(id: any){
            return this.http.delete(this.url +'/manga/deleteMangas/'+ id)
        }
    }
