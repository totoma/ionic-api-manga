import { Injectable } from '@angular/core';
import { Camera, CameraResultType, CameraSource, Photo } from '@capacitor/camera';
import { Filesystem, Directory } from '@capacitor/filesystem';

@Injectable({
  providedIn: 'root'
})
export class PhotoService {

  arrayPhoto = []

  constructor() { }

  /**sert  */
  private PHOTO_STORAGE: string = 'photos';

  //lors du clique en html on lance la méthode TakePhoto qui attend que la caméra envoie la photo grâce à la méthode getPhoto()
  public async takePhoto() {
    const photoTaken = await Camera.getPhoto({
      resultType: CameraResultType.Uri,
      source: CameraSource.Camera,
      quality: 100
    });

    // retour de savePicture() dans savedImageFile. FilePath = nom et WebView = donnée brut de la photo)
    await this.savePicture(photoTaken);
  }

  public async loadSaved() {
  
    const read = await Filesystem.readdir({
      path :"",
      directory: Directory.Data
    })
    console.log(read);
    
    // Affichez la photo en lisant au format base64
    for (let photo of read.files) {
      // Lire les données de chaque photo enregistrée à partir du système de fichiers
      const readFile = await Filesystem.readFile({
        path: photo.name,
        directory: Directory.Data,
      });
      
      this.arrayPhoto.unshift({
        filePath: photo.name,
        webViewPath: `data:image/jpeg;base64,${readFile.data}`
      })
    }

  }


  private async readAsBase64(photo: Photo) {
    // Récupérez la photo, lisez-la comme un blob, puis convertissez-la au format base64
    const response = await fetch(photo.webPath!);
    const blob = await response.blob();

    return await this.convertBlobToBase64(blob) as string;
  }

  /**
   * @param photo PhotoTaken
   */
  private async savePicture(photo: Photo) {
    // Convertir la photo au format base64, requis par l'API du système de fichiers pour enregistrer
    const base64Data = await this.readAsBase64(photo);

    // Écrire le fichier dans le répertoire de données
    const fileName = new Date().getTime() + '.jpeg';
    // method de FileSystem qui permet d'écrire sur l'appareil qui prend en propriété le nom, la donnée brut et le chemin qui s'adapte à l'OS
    const savedFile = await Filesystem.writeFile({
      path: fileName,
      data: base64Data,
      directory: Directory.Data
    });

    // Utilisez webPath pour afficher la nouvelle image au lieu de base64 car c'est
  //déjà chargé en mémoire
    return {
      filepath: fileName,
      webviewPath: photo.webPath
    };
  }




  /**instancie nouvel objet */

  private convertBlobToBase64 = (blob: Blob) => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onerror = reject;
    reader.onload = () => {
      resolve(reader.result);
    };
    reader.readAsDataURL(blob);
  });


  
}
