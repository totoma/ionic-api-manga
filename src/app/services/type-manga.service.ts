import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { TypeManga } from "../models/TypeManga";

@Injectable({
    providedIn: 'root'
})

export class typeMangaService {

    url = "http://localhost:8000"

    constructor(private http: HttpClient){
        this.http = http
    }

    getAllTypeManga(){
        return this.http.get(this.url + '/typeManga/getAllTypeMangas')
    }

    postData(data: any){
        return this.http.post(this.url + '/typeManga/createTypeManga', data)
    }

    updateTypeManga(id: any, data: any){
        return this.http.put(this.url+ '/typeManga/updateTypeManga'+ id, data)
    }

    // getTypeManga(id: any){
    //     return this.http.get(this.url+ '/typeManga/')
    // }

    deleteTypeManga(id: any){
        return this.http.delete(this.url + '/typeManga/deleteTypeMangas/' + id)
    }
}