import { TestBed } from '@angular/core/testing';
import { Manga } from '../models/Manga';
import { typeMangaService } from './type-manga.service';

describe('TypeMangaService', () => {
  let service: typeMangaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(typeMangaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
