import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailMangaPageRoutingModule } from './detail-manga-routing.module';

import { DetailMangaPage } from './detail-manga.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailMangaPageRoutingModule
  ],
  declarations: [DetailMangaPage]
})
export class DetailMangaPageModule {}
