import { Component, OnInit } from '@angular/core';
import { MangaService } from '../services/manga.service';
import { typeMangaService } from '../services/type-manga.service';



@Component({
  selector: 'app-detail-manga',
  templateUrl: './detail-manga.page.html',
  styleUrls: ['./detail-manga.page.scss'],
})
export class DetailMangaPage implements OnInit {

  resultGet: any;
  mangaShown: any = null;
  id: any;
  mangaTaken: any;
  mangaUrl: any;
  mangaName: any;
  mangaAuthor: any;

  constructor(public MangaService : MangaService, private typeMangaService: typeMangaService) { }

  ngOnInit() {
    this.MangaService.getManga(this.MangaService.idTaken).subscribe(res => {
      this.mangaTaken = res
    })    
}

  showManga(id: any){
    this.MangaService.getManga(id).subscribe((response: any) => {
      this.mangaShown = response.mangas;
    })
  }

  getManga(id: any){
    this.MangaService.getManga(id).subscribe(res=>{
      console.log(res);
      this.resultGet = res;
    })
  }


  updateManga(idUpdate: any){
    
    const body = {
      id: idUpdate,
      name: this.mangaName,
      author: this.mangaAuthor,
      url: this.mangaUrl
    }
    
    this.MangaService.updateManga(body).subscribe(response => {
      console.log(response);
      this.ngOnInit();
    })
  }

}
