import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailMangaPage } from './detail-manga.page';

const routes: Routes = [
  {
    path: '',
    component: DetailMangaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailMangaPageRoutingModule {}
